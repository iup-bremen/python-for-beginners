**********************************************
Python for beginners (in Earth System Science)
**********************************************

:Author:  Andreas Hilboll
:Contact: Andreas.Hilboll@uni-bremen.de
:Version: 0.5
:Date:    15 Feb 2017          

This repository contains the material for a three-day introductory course for
using Python in the Earth system (mostly atmospheric) sciences.
