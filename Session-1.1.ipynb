{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Session 1: Basic array operations\n",
    "\n",
    "Source: https://swcarpentry.github.io/python-novice-inflammation/01-numpy/\n",
    "\n",
    "Teaching: **30 min**, Exercises: **0 min**\n",
    "\n",
    "### Questions\n",
    "- How can I process tabular data files in Python?\n",
    "\n",
    "### Objectives\n",
    "- Explain what a library is, and what libraries are used for.\n",
    "- Import a Python library and use the functions it contains.\n",
    "- Read tabular data from a file into a program.\n",
    "- Assign values to variables.\n",
    "- Select individual values and subsections from data.\n",
    "- Perform operations on arrays of data.\n",
    "- Plot simple graphs from data."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Analyzing Patient Data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Words are useful,\n",
    "but what's more useful are the sentences and stories we build with them.\n",
    "Similarly,\n",
    "while a lot of powerful, general tools are built into languages like Python,\n",
    "specialized tools built up from these basic units live in [libraries](https://swcarpentry.github.io/python-novice-inflammation/reference/#library)\n",
    "that can be called upon when needed.\n",
    "\n",
    "In order to load our inflammation data,\n",
    "we need to access ([import](https://swcarpentry.github.io/python-novice-inflammation/reference/#import) in Python terminology)\n",
    "a library called [NumPy](http://docs.scipy.org/doc/numpy/ \"NumPy Documentation\").\n",
    "In general you should use this library if you want to do fancy things with numbers,\n",
    "especially if you have matrices or arrays.\n",
    "We can import NumPy using:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Importing a library is like getting a piece of lab equipment out of a storage locker and setting it up on the bench.\n",
    "Libraries provide additional functionality to the basic Python package,\n",
    "much like a new piece of equipment adds functionality to a lab space.\n",
    "Once you've imported the library,\n",
    "we can ask the library to read our data file for us:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "np.loadtxt(fname='data/inflammation-01.csv', delimiter=',')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The expression `numpy.loadtxt(...)` is a [function call](https://swcarpentry.github.io/python-novice-inflammation/reference/#function-call)\n",
    "that asks Python to run the [function](https://swcarpentry.github.io/python-novice-inflammation/reference/#function) `loadtxt` which belongs to the `numpy` library.\n",
    "This [dotted notation](https://swcarpentry.github.io/python-novice-inflammation/reference/#dotted-notation) is used everywhere in Python\n",
    "to refer to the parts of things as `thing.component`.\n",
    "\n",
    "`numpy.loadtxt` has two [parameters](https://swcarpentry.github.io/python-novice-inflammation/reference/#parameter):\n",
    "the name of the file we want to read,\n",
    "and the [delimiter](https://swcarpentry.github.io/python-novice-inflammation/reference/#delimiter) that separates values on a line.\n",
    "These both need to be character strings (or [strings](https://swcarpentry.github.io/python-novice-inflammation/reference/#string) for short),\n",
    "so we put them in quotes.\n",
    "\n",
    "When we are finished typing and press Shift+Enter,\n",
    "the notebook runs our command.\n",
    "Since we haven't told it to do anything else with the function's output,\n",
    "the notebook displays it.\n",
    "In this case,\n",
    "that output is the data we just loaded.\n",
    "By default,\n",
    "only a few rows and columns are shown\n",
    "(with `...` to omit elements when displaying big arrays).\n",
    "To save space,\n",
    "Python displays numbers as `1.` instead of `1.0`\n",
    "when there's nothing interesting after the decimal point.\n",
    "\n",
    "Our call to `numpy.loadtxt` read our file,\n",
    "but didn't save the data in memory.\n",
    "To do that,\n",
    "we need to [assign](https://swcarpentry.github.io/python-novice-inflammation/reference/#assignment) the array to a [variable](https://swcarpentry.github.io/python-novice-inflammation/reference/#variable).\n",
    "A variable is just a name for a value,\n",
    "such as `x`, `current_temperature`, or `subject_id`.\n",
    "Python's variables must begin with a letter and are [case sensitive](https://swcarpentry.github.io/python-novice-inflammation/reference/#case-sensitive).\n",
    "We can create a new variable by assigning a value to it using `=`.\n",
    "As an illustration,\n",
    "let's step back and instead of considering a table of data,\n",
    "consider the simplest \"collection\" of data,\n",
    "a single value.\n",
    "The line below assigns the value `55` to a variable `weight_kg`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "weight_kg = 55"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once a variable has a value, we can print it to the screen:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print(weight_kg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "and do arithmetic with it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print('weight in pounds:', 2.2 * weight_kg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As the example above shows,\n",
    "we can print several things at once by separating them with commas.\n",
    "\n",
    "We can also change a variable's value by assigning it a new one:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "weight_kg = 57.5\n",
    "print('weight in kilograms is now:', weight_kg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we imagine the variable as a sticky note with a name written on it,\n",
    "assignment is like putting the sticky note on a particular value:\n",
    "\n",
    "![Variables as Sticky Notes](img/python-sticky-note-variables-01.svg)\n",
    "\n",
    "This means that assigning a value to one variable does *not* change the values of other variables.\n",
    "For example,\n",
    "let's store the subject's weight in pounds in a variable:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "weight_lb = 2.2 * weight_kg\n",
    "print('weight in kilograms:', weight_kg, 'and in pounds:', weight_lb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![Creating Another Variable](img/python-sticky-note-variables-02.svg)\n",
    "\n",
    "and then change `weight_kg`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "weight_kg = 100.0\n",
    "print('weight in kilograms is now:', weight_kg,\n",
    "      'and weight in pounds is still:', weight_lb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![Updating a Variable](img/python-sticky-note-variables-03.svg)\n",
    "\n",
    "Since `weight_lb` doesn't \"remember\" where its value came from,\n",
    "it isn't automatically updated when `weight_kg` changes.\n",
    "This is different from the way spreadsheets work."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Just as we can assign a single value to a variable, we can also assign an array of values\n",
    "to a variable using the same syntax.  Let's re-run `numpy.loadtxt` and save its result:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "data = np.loadtxt(fname='data/inflammation-01.csv', delimiter=',')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This statement doesn't produce any output because assignment doesn't display anything.\n",
    "If we want to check that our data has been loaded,\n",
    "we can print the variable's value:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print(data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that our data is in memory,\n",
    "we can start doing things with it.\n",
    "First,\n",
    "let's ask what [type](https://swcarpentry.github.io/python-novice-inflammation/reference/#type) of thing `data` refers to:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print(type(data))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The output tells us that `data` currently refers to\n",
    "an N-dimensional array created by the NumPy library.\n",
    "These data correspond to arthritis patients' inflammation.\n",
    "The rows are the individual patients and the columns\n",
    "are their daily inflammation measurements."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can see what the array's [shape](https://swcarpentry.github.io/python-novice-inflammation/reference/#shape) is like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print(data.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This tells us that `data` has 60 rows and 40 columns. When we created the\n",
    "variable `data` to store our arthritis data, we didn't just create the array, we also\n",
    "created information about the array, called [members](https://swcarpentry.github.io/python-novice-inflammation/reference/#member) or\n",
    "attributes. This extra information describes `data` in\n",
    "the same way an adjective describes a noun.\n",
    "`data.shape` is an attribute  of `data` which describes the dimensions of `data`.\n",
    "We use the same dotted notation for the attributes of variables\n",
    "that we use for the functions in libraries\n",
    "because they have the same part-and-whole relationship.\n",
    "\n",
    "If we want to get a single number from the array,\n",
    "we must provide an [index](https://swcarpentry.github.io/python-novice-inflammation/reference/#index) in square brackets,\n",
    "just as we do in math:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print('first value in data:', data[0, 0])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print('middle value in data:', data[30, 20])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The expression `data[30, 20]` may not surprise you,\n",
    "but `data[0, 0]` might.\n",
    "Programming languages like Fortran and MATLAB start counting at 1,\n",
    "because that's what human beings have done for thousands of years.\n",
    "Languages in the C family (including C++, Java, Perl, and Python) count from 0\n",
    "because it represents an offset from the first value in the array (the second\n",
    "value is offset by one index from the first value). This is closer to the way\n",
    "that computers represent arrays (if you are interested in the historical\n",
    "reasons behind counting indices from zero, you can read\n",
    "[Mike Hoye's blog post](http://exple.tive.org/blarg/2013/10/22/citation-needed/)).\n",
    "As a result,\n",
    "if we have an M×N array in Python,\n",
    "its indices go from 0 to M-1 on the first axis\n",
    "and 0 to N-1 on the second.\n",
    "It takes a bit of getting used to,\n",
    "but one way to remember the rule is that\n",
    "the index is how many steps we have to take from the start to get the item we want."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "An index like `[30, 20]` selects a single element of an array,\n",
    "but we can select whole sections as well.\n",
    "For example,\n",
    "we can select the first ten days (columns) of values\n",
    "for the first four patients (rows) like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print(data[0:4, 0:10])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The [slice](https://swcarpentry.github.io/python-novice-inflammation/reference/#slice) `0:4` means,\n",
    "\"Start at index 0 and go up to, but not including, index 4.\"\n",
    "Again,\n",
    "the up-to-but-not-including takes a bit of getting used to,\n",
    "but the rule is that the difference between the upper and lower bounds is the number of values in the slice.\n",
    "\n",
    "We don't have to start slices at 0:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print(data[5:10, 0:10])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We also don't have to include the upper and lower bound on the slice.\n",
    "If we don't include the lower bound,\n",
    "Python uses 0 by default;\n",
    "if we don't include the upper,\n",
    "the slice runs to the end of the axis,\n",
    "and if we don't include either\n",
    "(i.e., if we just use ':' on its own),\n",
    "the slice includes everything:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "small = data[:3, 36:]\n",
    "print('small is:')\n",
    "print(small)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Arrays also know how to perform common mathematical operations on their values.\n",
    "The simplest operations with data are arithmetic:\n",
    "add, subtract, multiply, and divide.\n",
    " When you do such operations on arrays,\n",
    "the operation is done on each individual element of the array.\n",
    "Thus:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "doubledata = data * 2.0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "will create a new array `doubledata`\n",
    "whose elements have the value of two times the value of the corresponding elements in `data`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print('original:')\n",
    "print(data[:3, 36:])\n",
    "print('doubledata:')\n",
    "print(doubledata[:3, 36:])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If,\n",
    "instead of taking an array and doing arithmetic with a single value (as above)\n",
    "you did the arithmetic operation with another array of the same shape,\n",
    "the operation will be done on corresponding elements of the two arrays.\n",
    "Thus:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "tripledata = doubledata + data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "will give you an array where `tripledata[0,0]` will equal `doubledata[0,0]` plus `data[0,0]`,\n",
    "and so on for all other elements of the arrays."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print('tripledata:')\n",
    "print(tripledata[:3, 36:])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Often, we want to do more than add, subtract, multiply, and divide values of data.\n",
    "NumPy knows how to do more complex operations on arrays.\n",
    "If we want to find the average inflammation for all patients on all days,\n",
    "for example,\n",
    "we can ask NumPy to compute `data`'s mean value:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print(np.mean(data))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`mean` is a [function](https://swcarpentry.github.io/python-novice-inflammation/reference/#function) that takes\n",
    "an array as an [argument](https://swcarpentry.github.io/python-novice-inflammation/reference/#argument).\n",
    "If variables are nouns, functions are verbs:\n",
    "they do things with variables."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "NumPy has lots of useful functions that take an array as input.\n",
    "Let's use three of those functions to get some descriptive values about the dataset.\n",
    "We'll also use multiple assignment,\n",
    "a convenient Python feature that will enable us to do this all in one line."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "maxval, minval, stdval = np.max(data), np.min(data), np.std(data)\n",
    "\n",
    "print('maximum inflammation:', maxval)\n",
    "print('minimum inflammation:', minval)\n",
    "print('standard deviation:', stdval)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When analyzing data, though,\n",
    "we often want to look at partial statistics,\n",
    "such as the maximum value per patient\n",
    "or the average value per day.\n",
    "One way to do this is to create a new temporary array of the data we want,\n",
    "then ask it to do the calculation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "patient_0 = data[0, :] # 0 on the first axis, everything on the second\n",
    "print('maximum inflammation for patient 0:', patient_0.max())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Everything in a line of code following the '#' symbol is a\n",
    "[comment](https://swcarpentry.github.io/python-novice-inflammation/reference/#comment) that is ignored by the computer.\n",
    "Comments allow programmers to leave explanatory notes for other\n",
    "programmers or their future selves.\n",
    "\n",
    "We don't actually need to store the row in a variable of its own.\n",
    "Instead, we can combine the selection and the function call:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print('maximum inflammation for patient 2:', np.max(data[2, :]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What if we need the maximum inflammation for each patient over all days (as in the\n",
    "next diagram on the left), or the average for each day (as in the\n",
    "diagram on the right)? As the diagram below shows, we want to perform the\n",
    "operation across an axis:\n",
    "\n",
    "![Operations Across Axes](img/python-operations-across-axes.png)\n",
    "\n",
    "To support this,\n",
    "most array functions allow us to specify the axis we want to work on.\n",
    "If we ask for the average across axis 0 (rows in our 2D example),\n",
    "we get:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print(np.mean(data, axis=0))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As a quick check,\n",
    "we can ask this array what its shape is:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print(np.mean(data, axis=0).shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The expression `(40,)` tells us we have an N×1 vector,\n",
    "so this is the average inflammation per day for all patients.\n",
    "If we average across axis 1 (columns in our 2D example), we get:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print(np.mean(data, axis=1))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "which is the average inflammation per patient across all days.\n",
    "\n",
    "The mathematician Richard Hamming once said,\n",
    "\"The purpose of computing is insight, not numbers,\"\n",
    "and the best way to develop insight is often to visualize data.\n",
    "Visualization deserves an entire lecture (of course) of its own,\n",
    "but we can explore a few features of Python's `matplotlib` library here.\n",
    "While there is no \"official\" plotting library,\n",
    "this package is the de facto standard.\n",
    "First,\n",
    "we will import the `pyplot` module from `matplotlib`\n",
    "and use two of its functions to create and display a heat map of our data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "image = plt.imshow(data)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Blue regions in this heat map are low values, while red shows high values.\n",
    "As we can see,\n",
    "inflammation rises and falls over a 40-day period."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's take a look at the average inflammation over time:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "ave_inflammation = np.mean(data, axis=0)\n",
    "ave_plot = plt.plot(ave_inflammation)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here,\n",
    "we have put the average per day across all patients in the variable `ave_inflammation`,\n",
    "then asked `matplotlib.pyplot` to create and display a line graph of those values.\n",
    "The result is roughly a linear rise and fall,\n",
    "which is suspicious:\n",
    "based on other studies,\n",
    "we expect a sharper rise and slower fall.\n",
    "Let's have a look at two other statistics:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "max_plot = plt.plot(np.max(data, axis=0))\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The maximum value rises and falls perfectly smoothly,\n",
    "while the minimum seems to be a step function.\n",
    "Neither result seems particularly likely,\n",
    "so either there's a mistake in our calculations\n",
    "or something is wrong with our data.\n",
    "This insight would have been difficult to reach by\n",
    "examining the data without visualization tools.\n",
    "\n",
    "You can group similar plots in a single figure using subplots.\n",
    "This script below uses a number of new commands. The function `matplotlib.pyplot.figure()`\n",
    "creates a space into which we will place all of our plots. The parameter `figsize`\n",
    "tells Python how big to make this space. Each subplot is placed into the figure using\n",
    "its `add_subplot` [method](https://swcarpentry.github.io/python-novice-inflammation/reference/#method). The `add_subplot` method takes 3 parameters. The first denotes\n",
    "how many total rows of subplots there are, the second parameter refers to the\n",
    "total number of subplot columns, and the final parameter denotes which subplot\n",
    "your variable is referencing (left-to-right, top-to-bottom). Each subplot is stored in a\n",
    "different variable (`axes1`, `axes2`, `axes3`). Once a subplot is created, the axes can\n",
    "be titled using the `set_xlabel()` command (or `set_ylabel()`).\n",
    "Here are our three plots side by side:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "data = np.loadtxt(fname='data/inflammation-01.csv', delimiter=',')\n",
    "\n",
    "fig = plt.figure(figsize=(10.0, 3.0))\n",
    "\n",
    "axes1 = fig.add_subplot(1, 3, 1)\n",
    "axes2 = fig.add_subplot(1, 3, 2)\n",
    "axes3 = fig.add_subplot(1, 3, 3)\n",
    "\n",
    "axes1.set_ylabel('average')\n",
    "axes1.plot(np.mean(data, axis=0))\n",
    "\n",
    "axes2.set_ylabel('max')\n",
    "axes2.plot(np.max(data, axis=0))\n",
    "\n",
    "axes3.set_ylabel('min')\n",
    "axes3.plot(np.min(data, axis=0))\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The [call](https://swcarpentry.github.io/python-novice-inflammation/reference/#function-call) to `loadtxt` reads our data,\n",
    "and the rest of the program tells the plotting library\n",
    "how large we want the figure to be,\n",
    "that we're creating three subplots,\n",
    "what to draw for each one,\n",
    "and that we want a tight layout.\n",
    "(Perversely,\n",
    "if we leave out that call to `fig.tight_layout()`,\n",
    "the graphs will actually be squeezed together more closely.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise: Check your understanding\n",
    "\n",
    "Draw diagrams showing what variables refer to what values after each statement in the following program:\n",
    "\n",
    "    mass = 47.5\n",
    "    age = 122\n",
    "    mass = mass * 2.0\n",
    "    age = age - 20"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise: Sorting out references\n",
    "\n",
    "What does the following program print out?\n",
    "\n",
    "    first, second = 'Grace', 'Hopper'\n",
    "    third, fourth = second, first\n",
    "    print(third, fourth)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise: Slicing Strings\n",
    "A section of an array is called a [slice](https://swcarpentry.github.io/python-novice-inflammation//reference/#slice). We can take slices of character strings as well:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "element = 'oxygen'\n",
    "print('first three characters:', element[0:3])\n",
    "print('last three characters:', element[3:6])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What is the value of `element[:4]`? What about `element[4:]`? Or `element[:]`?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What is `element[-1]`? What is `element[-2]`?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Given those answers, explain what `element[1:-1]` does."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise: Thin Slices\n",
    "\n",
    "The expression `element[3:3]` produces an [empty string](https://swcarpentry.github.io/python-novice-inflammation//reference/#empty-string),\n",
    "i.e., a string that contains no characters.\n",
    "If `data` holds our array of patient data,\n",
    "what does `data[3:3, 4:4]` produce?\n",
    "What about `data[3:3, :]`?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise: Plot Scaling\n",
    "\n",
    "1. Why do all of our plots stop just short of the upper end of our graph?\n",
    "2. If we want to change this, we can use the `set_ylim(min, max)` method of each 'axes', for example:\n",
    "\n",
    "       axes3.set_ylim(0,6)\n",
    "\n",
    "   Update your plotting code to automatically set a more appropriate scale. (Hint: you can make use of the `max` and `min` methods to help.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise: Drawing Straight Lines\n",
    "In the center and right subplots above, we expect all lines to look like step functions, because non-integer value are not realistic for the minimum and maximum values. However, you can see that the lines are not always vertical or horizontal, and in particular the step function in the subplot on the right looks slanted. Why is this?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise: Make Your Own Plot\n",
    "Create a plot showing the standard deviation (`numpy.std`) of the inflammation data for each day across all patients."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise: Moving plots around\n",
    "\n",
    "Modify the program to display the three plots on top of one another instead of side by side."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise: Stacking Arrays\n",
    "\n",
    "Arrays can be concatenated and stacked on top of one another, using NumPy’s `vstack` and `hstack` functions for vertical and horizontal stacking, respectively."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "A = np.array([[1,2,3], [4,5,6], [7, 8, 9]])\n",
    "print('A = ')\n",
    "print(A)\n",
    "\n",
    "B = np.hstack([A, A])\n",
    "print('B = ')\n",
    "print(B)\n",
    "\n",
    "C = np.vstack([A, A])\n",
    "print('C = ')\n",
    "print(C)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Write some additional code that slices the first and last columns of `A`, and stacks them into a 3x2 array. Make sure to `print` the results to verify your solution."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise: Change In Inflamation\n",
    "This patient data is longitudinal in the sense that each row represents a series of observations relating to one individual. This means that change inflamation is a meaningful concept.\n",
    "\n",
    "The `numpy.diff()` function takes a NumPy array and returns the difference along a specified axis.\n",
    "\n",
    "1. Which axis would it make sense to use this function along?\n",
    "2. If the shape of an individual data file is (60, 40) (60 rows and 40 columns), what would the shape of the array be after you run the diff() function and why?\n",
    "3. How would you find the largest change in inflammation for each patient? Does it matter if the change in inflammation is an increase or a decrease?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Key Points\n",
    "- Import a library into a program using `import libraryname`.\n",
    "- Use the `numpy` library to work with arrays in Python.\n",
    "- Use `variable = value` to assign a value to a variable in order to record it in memory.\n",
    "- Variables are created on demand whenever a value is assigned to them.\n",
    "- Use `print(something)` to display the value of `something`.\n",
    "- The expression `array.shape` gives the shape of an array.\n",
    "- Use `array[x, y]` to select a single element from an array.\n",
    "- Array indices start at 0, not 1.\n",
    "- Use `low:high` to specify a slice that includes the indices from `low` to `high-1`.\n",
    "- All the indexing and slicing that works on arrays also works on strings.\n",
    "- Use `# some kind of explanation` to add comments to programs.\n",
    "- Use `numpy.mean(array)`, `numpy.max(array)`, and `numpy.min(array)` to calculate simple statistics.\n",
    "- Use `numpy.mean(array, axis=0)` or `numpy.mean(array, axis=1)` to calculate statistics across the specified axis.\n",
    "- Use the `pyplot` library from `matplotlib` for creating simple visualizations.\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
