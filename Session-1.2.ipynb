{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Session 1.2: Repeating actions with loops\n",
    "\n",
    "Source: https://swcarpentry.github.io/python-novice-inflammation/02-loop/\n",
    "\n",
    "Teaching: **30 min**, Exercises: **0 min**\n",
    "\n",
    "### Questions\n",
    "- How can I do the same operations on many different values?\n",
    "\n",
    "### Objectives\n",
    "- Explain what a for loop does.\n",
    "- Correctly write for loops to repeat simple calculations.\n",
    "- Trace changes to a loop variable as the loop runs.\n",
    "- Trace changes to other variables as they are updated by a for loop."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the last lesson,\n",
    "we wrote some code that plots some values of interest from our first inflammation dataset,\n",
    "and reveals some suspicious features in it, such as from `inflammation-01.csv`\n",
    "\n",
    "![Analysis of inflammation-01.csv](img/03-loop_2_0.png)\n",
    "\n",
    "We have a dozen data sets right now, though, and more on the way.\n",
    "We want to create plots for all of our data sets with a single statement.\n",
    "To do that, we'll have to teach the computer how to repeat things.\n",
    "\n",
    "An example task that we might want to repeat is printing each character in a\n",
    "word on a line of its own."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "word = 'lead'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can access a character in a string using its index. For example, we can get the first\n",
    "character of the word 'lead', by using `word[0]`. One way to print each character is to use\n",
    "four `print` statements:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print(word[0])\n",
    "print(word[1])\n",
    "print(word[2])\n",
    "print(word[3])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is a bad approach for two reasons:\n",
    "\n",
    "1.  It doesn't scale:\n",
    "    if we want to print the characters in a string that's hundreds of letters long,\n",
    "    we'd be better off just typing them in.\n",
    "\n",
    "2.  It's fragile:\n",
    "    if we give it a longer string,\n",
    "    it only prints part of the data,\n",
    "    and if we give it a shorter one,\n",
    "    it produces an error because we're asking for characters that don't exist."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "word = 'tin'\n",
    "print(word[0])\n",
    "print(word[1])\n",
    "print(word[2])\n",
    "print(word[3])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here's a better approach:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "word = 'lead'\n",
    "for char in word:\n",
    "    print(char)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is shorter - certainly shorter than something that prints every character in a hundred-letter string - and\n",
    "more robust as well:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "word = 'oxygen'\n",
    "for char in word:\n",
    "    print(char)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The improved version uses a [for loop](https://swcarpentry.github.io/python-novice-inflammation//reference/#for-loop)\n",
    "to repeat an operation - in this case, printing - once for each thing in a sequence.\n",
    "The general form of a loop is:\n",
    "\n",
    "    for element in variable:\n",
    "        do things with element\n",
    "\n",
    "Using the oxygen example above, the loop might look like this:\n",
    "\n",
    "![loop_image](img/loops_image.png)\n",
    "\n",
    "where each character (`char`) in the variable `word` is looped through and printed one character after another.\n",
    "The numbers in the diagram denote which loop cycle the character was printed in (1 being the first loop, and 6 being the final loop).\n",
    "\n",
    "We can call the [loop variable](https://swcarpentry.github.io/python-novice-inflammation//reference/#loop-variable) anything we like,\n",
    "but there must be a colon at the end of the line starting the loop,\n",
    "and we must indent anything we want to run inside the loop. Unlike many other languages, there is no\n",
    "command to signify the end of the loop body (e.g. end for); what is indented after the for statement belongs to the loop."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here's another loop that repeatedly updates a variable:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "length = 0\n",
    "for vowel in 'aeiou':\n",
    "    length = length + 1\n",
    "print('There are', length, 'vowels')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It's worth tracing the execution of this little program step by step.\n",
    "Since there are five characters in `'aeiou'`,\n",
    "the statement on line 3 will be executed five times.\n",
    "The first time around,\n",
    "`length` is zero (the value assigned to it on line 1)\n",
    "and `vowel` is `'a'`.\n",
    "The statement adds 1 to the old value of `length`,\n",
    "producing 1,\n",
    "and updates `length` to refer to that new value.\n",
    "The next time around,\n",
    "`vowel` is `'e'` and `length` is 1,\n",
    "so `length` is updated to be 2.\n",
    "After three more updates,\n",
    "`length` is 5;\n",
    "since there is nothing left in `'aeiou'` for Python to process,\n",
    "the loop finishes\n",
    "and the `print` statement on line 4 tells us our final answer.\n",
    "\n",
    "Note that a loop variable is just a variable that's being used to record progress in a loop.\n",
    "It still exists after the loop is over,\n",
    "and we can re-use variables previously defined as loop variables as well:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "letter = 'z'\n",
    "for letter in 'abc':\n",
    "    print(letter)\n",
    "print('after the loop, letter is', letter)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note also that finding the length of a string is such a common operation\n",
    "that Python actually has a built-in function to do it called `len`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print(len('aeiou'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`len` is much faster than any function we could write ourselves,\n",
    "and much easier to read than a two-line loop;\n",
    "it will also give us the length of many other things that we haven't met yet,\n",
    "so we should always use it when we can."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise: From 1 to N\n",
    "\n",
    "Python has a built-in function called `range` that creates a sequence of numbers. Range can\n",
    "accept 1-3 parameters. If one parameter is input, range creates an array of that length,\n",
    "starting at zero and incrementing by 1. If 2 parameters are input, range starts at\n",
    "the first and ends just before the second, incrementing by one. If range is passed 3 parameters,\n",
    "it starts at the first one, ends just before the second one, and increments by the third one. For\n",
    "example,\n",
    "`range(3)` produces the numbers 0, 1, 2, while `range(2, 5)` produces 2, 3, 4,\n",
    "and `range(3, 10, 3)` produces 3, 6, 9.\n",
    "\n",
    "Using `range`,\n",
    "write a loop that uses `range` to print the first 3 natural numbers:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise: Computing Powers With Loops\n",
    "\n",
    "Exponentiation is built into Python:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print(5 ** 3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Write a loop that calculates the same result as `5 ** 3` using\n",
    "multiplication (and without exponentiation)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise: Reverse a String\n",
    "\n",
    "Write a loop that takes a string,\n",
    "and produces a new string with the characters in reverse order,\n",
    "so `'Newton'` becomes `'notweN'`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise: Computing the Value of a Polynomial\n",
    "\n",
    "The built-in function `enumerate` takes a sequence (e.g. a list) and generates a\n",
    "new sequence of the same length. Each element of the new sequence contains the index\n",
    "(0,1,2,...) and the value from the original sequence:\n",
    "\n",
    "    for i, x in enumerate(xs):\n",
    "        # Do something with i and x\n",
    "\n",
    "The loop above assigns the index to `i` and the value to `x`.\n",
    "\n",
    "Suppose you have encoded a polynomial as a list of coefficients in\n",
    "the following way: the first element is the constant term, the\n",
    "second element is the coefficient of the linear term, the third is the\n",
    "coefficient of the quadratic term, etc.\n",
    "\n",
    "    x = 5\n",
    "    cc = [2, 4, 3]\n",
    "\n",
    "where\n",
    "\n",
    "    y = cc[0] * x**0 + cc[1] * x**1 + cc[2] * x**2\n",
    "    y = 97\n",
    "\n",
    "Write a loop using `enumerate(cc)` which computes the value `y` of any\n",
    "polynomial, given `x` and `cc`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Key Points\n",
    "- Use `for variable in sequence` to process the elements of a sequence one at a time.\n",
    "- The body of a for loop must be indented.\n",
    "- Use `len(thing)` to determine the length of something that contains other values.\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
