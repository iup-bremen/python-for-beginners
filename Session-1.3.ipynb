{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Session 1.3: Storing Multiple Values in Lists\n",
    "\n",
    "Source: https://swcarpentry.github.io/python-novice-inflammation/03-lists/\n",
    "\n",
    "Teaching: **30 min**, Exercises: **0 min**\n",
    "\n",
    "### Questions\n",
    "- How can I store many values together?\n",
    "\n",
    "### Objectives\n",
    "- Explain what a list is.\n",
    "- Create and index lists of simple values."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Just as a `for` loop is a way to do operations many times,\n",
    "a list is a way to store many values.\n",
    "Unlike NumPy arrays,\n",
    "lists are built into the language (so we don't have to load a library\n",
    "to use them).\n",
    "We create a list by putting values inside square brackets and separating the values with commas:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "odds = [1, 3, 5, 7]\n",
    "print('odds are:', odds)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We select individual elements from lists by indexing them:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print('first and last:', odds[0], odds[-1])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "and if we loop over a list,\n",
    "the loop variable is assigned elements one at a time:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "for number in odds:\n",
    "    print(number)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There is one important difference between lists and strings:\n",
    "we can change the values in a list,\n",
    "but we cannot change individual characters in a string.\n",
    "For example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "names = ['Newton', 'Darwing', 'Turing'] # typo in Darwin's name\n",
    "print('names is originally:', names)\n",
    "names[1] = 'Darwin' # correct the name\n",
    "print('final value of names:', names)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "works, but:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "name = 'Darwin'\n",
    "name[0] = 'd'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "does not."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Since lists can contain any Python variable, it can even contain other lists.\n",
    "\n",
    "For example, we could represent the products in the shelves of a small grocery shop:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "x = [['pepper', 'zucchini', 'onion'],\n",
    "     ['cabbage', 'lettuce', 'garlic'],\n",
    "     ['apple', 'pear', 'banana']]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is a visual example of how indexing a list of lists `x` works:\n",
    "\n",
    "![The first element of a list. Adapted from @hadleywickham's tweet about R lists.](img/indexing_lists_python.png)\n",
    "([Source](https://twitter.com/hadleywickham/status/643381054758363136))\n",
    "\n",
    "Using the previously declared list `x`, these would be the results of the\n",
    "index operations shown in the image:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print([x[0]])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print(x[0])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print(x[0][0])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are many ways to change the contents of lists besides assigning new values to\n",
    "individual elements:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "odds.append(11)\n",
    "print('odds after adding a value:', odds)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "del odds[0]\n",
    "print('odds after removing the first element:', odds)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "odds.reverse()\n",
    "print('odds after reversing:', odds)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "While modifying in place, it is useful to remember that Python treats lists in a slightly counter-intuitive way.\n",
    "\n",
    "If we make a list and (attempt to) copy it then modify in place, we can cause all sorts of trouble:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "odds = [1, 3, 5, 7]\n",
    "primes = odds\n",
    "primes += [2]\n",
    "print('primes:', primes)\n",
    "print('odds:', odds)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is because Python stores a list in memory, and then can use multiple names to refer to the same list.\n",
    "If all we want to do is copy a (simple) list, we can use the `list` function, so we do not modify a list we did not mean to:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "odds = [1, 3, 5, 7]\n",
    "primes = list(odds)\n",
    "primes += [2]\n",
    "print('primes:', primes)\n",
    "print('odds:', odds)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is different from how variables worked in session 1.1, and more similar to how a spreadsheet works."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise: Turn a String Into a List\n",
    "\n",
    "Use a for-loop to convert the string \"hello\" into a list of letters:\n",
    "\n",
    "    [\"h\", \"e\", \"l\", \"l\", \"o\"]\n",
    "\n",
    "Hint: You can create an empty list like this:\n",
    "\n",
    "    my_list = []"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Subsets of lists and strings can be accessed by specifying ranges of values in brackets,\n",
    "similar to how we accessed ranges of positions in a Numpy array.\n",
    "This is commonly referred to as \"slicing\" the list/string."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "binomial_name = \"Drosophila melanogaster\"\n",
    "group = binomial_name[0:10]\n",
    "print(\"group:\", group)\n",
    "\n",
    "species = binomial_name[11:24]\n",
    "print(\"species:\", species)\n",
    "\n",
    "chromosomes = [\"X\", \"Y\", \"2\", \"3\", \"4\"]\n",
    "autosomes = chromosomes[2:5]\n",
    "print(\"autosomes:\", autosomes)\n",
    "\n",
    "last = chromosomes[-1]\n",
    "print(\"last:\", last)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise: Slicing From the End\n",
    "\n",
    "Use slicing to access only the last four characters of a string or entries of a list.\n",
    "\n",
    "    string_for_slicing = \"Observation date: 02-Feb-2013\"\n",
    "    list_for_slicing = [[\"fluorine\", \"F\"], [\"chlorine\", \"Cl\"], [\"bromine\", \"Br\"], [\"iodine\", \"I\"], [\"astatine\", \"At\"]]\n",
    "\n",
    "e.g.,\n",
    "\n",
    "    \"2013\"\n",
    "    [[\"chlorine\", \"Cl\"], [\"bromine\", \"Br\"], [\"iodine\", \"I\"], [\"astatine\", \"At\"]]\n",
    "\n",
    "Would your solution work regardless of whether you knew beforehand\n",
    "the length of the string or list\n",
    "(e.g. if you wanted to apply the solution to a set of lists of different lengths)?\n",
    "If not, try to change your approach to make it more robust."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise: Non-Continuous Slices\n",
    "\n",
    "So far we've seen how to use slicing to take single blocks\n",
    "of successive entries from a sequence.\n",
    "But what if we want to take a subset of entries\n",
    "that aren't next to each other in the sequence?\n",
    "\n",
    "You can achieve this by providing a third argument\n",
    "to the range within the brackets, called the _step size_.\n",
    "The example below shows how you can take every third entry in a list:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "primes = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37]\n",
    "subset = primes[0:12:3]\n",
    "print(\"subset\", subset)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice that the slice taken begins with the first entry in the range,\n",
    "followed by entries taken at equally-spaced intervals (the steps) thereafter.\n",
    "If you wanted to begin the subset with the third entry,\n",
    "you would need to specify that as the starting point of the sliced range:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "primes = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37]\n",
    "subset = primes[2:12:3]\n",
    "print(\"subset\", subset)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Use the step size argument to create a new string\n",
    "that contains only every other character in the string\n",
    "\"In an octopus's garden in the shade\", so that the result is \n",
    "\n",
    "    I notpssgre ntesae"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "beatles = \"In an octopus's garden in the shade\"\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you want to take a slice from the beginning of a sequence, you can omit the first index in the range:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "date = \"Monday 4 January 2016\"\n",
    "day = date[0:6]\n",
    "print(\"Using 0 to begin range:\", day)\n",
    "day = date[:6]\n",
    "print(\"Omitting beginning index:\", day)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And similarly, you can omit the ending index in the range to take a slice to the very end of the sequence:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "months = [\"jan\", \"feb\", \"mar\", \"apr\", \"may\", \"jun\", \"jul\",\n",
    "          \"aug\", \"sep\", \"oct\", \"nov\", \"dec\"]\n",
    "sond = months[8:12]\n",
    "print(\"With known last position:\", sond)\n",
    "sond = months[8:len(months)]\n",
    "print(\"Using len() to get last entry:\", sond)\n",
    "sond = months[8:]\n",
    "print(\"Omitting ending index:\", sond)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise: Swapping the contents of variables\n",
    "\n",
    "Explain what the overall effect of this code is:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "left = 'L'\n",
    "right = 'R'\n",
    "\n",
    "temp = left\n",
    "left = right\n",
    "right = temp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Compare it to:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "left, right = [right, left]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Do they always do the same thing? Which do you find easier to read?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise: Overloading\n",
    "\n",
    "`+` usually means addition, but when used on strings or lists, it means \"concatenate\".\n",
    "Given that, what do you think the multiplication operator `*` does on lists?\n",
    "In particular, what will be the output of the following code?\n",
    "\n",
    "    counts = [2, 4, 6, 8, 10]\n",
    "    repeats = counts * 2\n",
    "    print(repeats)\n",
    "\n",
    "1.  `[2, 4, 6, 8, 10, 2, 4, 6, 8, 10]`\n",
    "2.  `[4, 8, 12, 16, 20]`\n",
    "3.  `[[2, 4, 6, 8, 10],[2, 4, 6, 8, 10]]`\n",
    "4.  `[2, 4, 6, 8, 10, 4, 8, 12, 16, 20]`\n",
    "\n",
    "The technical term for this is *operator overloading*:\n",
    "a single operator, like `+` or `*`,\n",
    "can do different things depending on what it's applied to."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Key Points\n",
    "\n",
    "- `[value1, value2, value3, ...]` creates a list.\n",
    "- Lists are indexed and sliced in the same way as strings and arrays.\n",
    "- Lists are mutable (i.e., their values can be changed in place).\n",
    "- Strings are immutable (i.e., the characters in them cannot be changed)."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
