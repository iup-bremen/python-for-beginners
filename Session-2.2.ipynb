{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Session 2.2: Making choices\n",
    "\n",
    "Source: https://swcarpentry.github.io/python-novice-inflammation/05-cond/\n",
    "        \n",
    "Teaching: **30 min**, Exercises: **0 min**\n",
    "\n",
    "### Questions\n",
    "- How can my programs do different things based on data values?\n",
    "\n",
    "### Objectives\n",
    "- Write conditional statements including `if`, `elif`, and `else` branches.\n",
    "- Correctly evaluate expressions containing `and` and `or`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In our last lesson, we discovered something suspicious was going on\n",
    "in our inflammation data by drawing some plots.\n",
    "How can we use Python to automatically recognize the different features we saw,\n",
    "and take a different action for each? In this lesson, we'll learn how to write code that\n",
    "runs only when certain conditions are true."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Conditionals\n",
    "\n",
    "We can ask Python to take different actions, depending on a condition, with an `if` statement:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "num = 37\n",
    "if num > 100:\n",
    "    print('greater')\n",
    "else:\n",
    "    print('not greater')\n",
    "print('done')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The second line of this code uses the keyword `if` to tell Python that we want to make a choice.\n",
    "If the test that follows the `if` statement is true,\n",
    "the body of the `if`\n",
    "(i.e., the lines indented underneath it) are executed.\n",
    "If the test is false,\n",
    "the body of the `else` is executed instead.\n",
    "Only one or the other is ever executed:\n",
    "\n",
    "![Executing a Conditional](img/python-flowchart-conditional.png)\n",
    "\n",
    "Conditional statements don't have to include an `else`.\n",
    "If there isn't one,\n",
    "Python simply does nothing if the test is false:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "num = 53\n",
    "print('before conditional...')\n",
    "if num > 100:\n",
    "    print('53 is greater than 100')\n",
    "print('...after conditional')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also chain several tests together using `elif`,\n",
    "which is short for \"else if\".\n",
    "The following Python code uses `elif` to print the sign of a number."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "num = -3\n",
    "\n",
    "if num > 0:\n",
    "    print(num, \"is positive\")\n",
    "elif num == 0:\n",
    "    print(num, \"is zero\")\n",
    "else:\n",
    "    print(num, \"is negative\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One important thing to notice in the code above is that we use a double equals sign `==` to test for equality\n",
    "rather than a single equals sign\n",
    "because the latter is used to mean assignment.\n",
    "\n",
    "We can also combine tests using `and` and `or`.\n",
    "`and` is only true if both parts are true:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "if (1 > 0) and (-1 > 0):\n",
    "    print('both parts are true')\n",
    "else:\n",
    "    print('at least one part is false')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "while `or` is true if at least one part is true:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "if (1 < 0) or (-1 < 0):\n",
    "    print('at least one test is true')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Checking our Data\n",
    "\n",
    "Now that we've seen how conditionals work,\n",
    "we can use them to check for the suspicious features we saw in our inflammation data.\n",
    "In the first couple of plots, the maximum inflammation per day\n",
    "seemed to rise like a straight line, one unit per day.\n",
    "We can check for this inside the `for` loop we wrote with the following conditional:\n",
    "\n",
    "    if numpy.max(data, axis=0)[0] == 0 and numpy.max(data, axis=0)[20] == 20:\n",
    "        print('Suspicious looking maxima!')\n",
    "        \n",
    "We also saw a different problem in the third dataset;\n",
    "the minima per day were all zero (looks like a healthy person snuck into our study).\n",
    "We can also check for this with an `elif` condition:\n",
    "\n",
    "    elif numpy.sum(numpy.min(data, axis=0)) == 0:\n",
    "        print('Minima add up to zero!')\n",
    "\n",
    "And if neither of these conditions are true, we can use `else` to give the all-clear:\n",
    "\n",
    "    else:\n",
    "        print('Seems OK!')\n",
    "\n",
    "Let's test that out:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "data = np.loadtxt(fname='data/inflammation-01.csv', delimiter=',')\n",
    "\n",
    "if np.max(data, axis=0)[0] == 0 and np.max(data, axis=0)[20] == 20:\n",
    "    print('Suspicious looking maxima!')\n",
    "elif np.sum(np.min(data, axis=0)) == 0:\n",
    "    print('Minima add up to zero!')\n",
    "else:\n",
    "    print('Seems OK!')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "data = np.loadtxt(fname='data/inflammation-03.csv', delimiter=',')\n",
    "if np.max(data, axis=0)[0] == 0 and np.max(data, axis=0)[20] == 20:\n",
    "    print('Suspicious looking maxima!')\n",
    "elif np.sum(np.min(data, axis=0)) == 0:\n",
    "    print('Minima add up to zero!')\n",
    "else:\n",
    "    print('Seems OK!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this way,\n",
    "we have asked Python to do something different depending on the condition of our data.\n",
    "Here we printed messages in all cases,\n",
    "but we could also imagine not using the `else` catch-all\n",
    "so that messages are only printed when something is wrong,\n",
    "freeing us from having to manually examine every plot for features we've seen before."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise: How Many Paths?\n",
    "\n",
    "Look at the following piece of code:\n",
    "\n",
    "    if 4 > 5:\n",
    "        print('A')\n",
    "    elif 4 == 5:\n",
    "        print('B')\n",
    "    elif 4 < 5:\n",
    "        print('C')\n",
    "\n",
    "Which of the following would be printed if you were to run this code?\n",
    "Why did you pick this answer?\n",
    "\n",
    "1.  A\n",
    "2.  B\n",
    "3.  C\n",
    "4.  B and C"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "### Exercise: What Is Truth?\n",
    "\n",
    "`True` and `False` are special words in Python called `booleans`\n",
    "which represent true and false statements.\n",
    "However, they aren't the only values in Python that are true and false.\n",
    "In fact, *any* value can be used in an `if` or `elif`.\n",
    "After reading and running the code below,\n",
    "explain what the rule is for which values are considered true and which are considered false."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "if '':\n",
    "    print('empty string is true')\n",
    "if 'word':\n",
    "    print('word is true')\n",
    "if []:\n",
    "    print('empty list is true')\n",
    "if [1, 2, 3]:\n",
    "    print('non-empty list is true')\n",
    "if 0:\n",
    "    print('zero is true')\n",
    "if 1:\n",
    "    print('one is true')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "### Exercise: That's Not Not What I Meant\n",
    "\n",
    "Sometimes it is useful to check whether some condition is not true.\n",
    "The Boolean operator `not` can do this explicitly.\n",
    "After reading and running the code below,\n",
    "write some `if` statements that use `not` to test the rule\n",
    "that you formulated in the previous challenge."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "if not '':\n",
    "    print('empty string is not true')\n",
    "if not 'word':\n",
    "    print('word is not true')\n",
    "if not not True:\n",
    "    print('not not True is true')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "### Exercise: Close Enough\n",
    "\n",
    "Write some conditions that print `True` if the variable `a` is within 10% of the variable `b`\n",
    "and `False` otherwise.\n",
    "Compare your implementation with your partner's:\n",
    "do you get the same answer for all possible pairs of numbers?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "### Exercise: In-Place Operators\n",
    "\n",
    "Python (and most other languages in the C family) provides [in-place operators](https://swcarpentry.github.io/python-novice-inflammation//reference/#in-place-operators)\n",
    "such that code like\n",
    "\n",
    "    x = 1  # original value\n",
    "    x += 1 # add one to x, assigning result back to x\n",
    "    x *= 3 # multiply x by 3\n",
    "    print(x)\n",
    "\n",
    "will produce the output\n",
    "\n",
    "    6\n",
    "\n",
    "Write some code that sums the positive and negative numbers in a list separately,\n",
    "using in-place operators.\n",
    "Do you think the result is more or less readable than writing the same without in-place operators?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "### Exercise: Sorting a List Into Buckets\n",
    "\n",
    "The folder containing our data files has large data sets whose names start with\n",
    "\"inflammation-\", small ones whose names with \"small-\", and possibly other files\n",
    "whose sizes we don't know.  Our goal is to sort those files into three lists\n",
    "called `large_files`, `small_files`, and `other_files` respectively.  Add code\n",
    "to the template below to do this.  Note that the string method\n",
    "[`startswith`](https://docs.python.org/3.5/library/stdtypes.html#str.startswith)\n",
    "returns `True` if and only if the string it is called on starts with the string\n",
    "passed as an argument.\n",
    "\n",
    "    files = ['inflammation-01.csv', 'myscript.py', 'inflammation-02.csv', 'small-01.csv', 'small-02.csv']\n",
    "    large_files = []\n",
    "    small_files = []\n",
    "    other_files = []\n",
    "\n",
    "Your solution should:\n",
    "\n",
    "1.  loop over the names of the files\n",
    "2.  figure out which group each filename belongs\n",
    "3.  append the filename to that list\n",
    "\n",
    "In the end the three lists should be:\n",
    "\n",
    "    large_files = ['inflammation-01.csv', 'inflammation-02.csv']\n",
    "    small_files = ['small-01.csv', 'small-02.csv']\n",
    "    other_files = ['myscript.py']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "### Exercise: Counting Vowels\n",
    "\n",
    "1.  Write a loop that counts the number of vowels in a character string.\n",
    "2. Test it on a few individual words and full sentences.\n",
    "3. Once you are done, compare your solution to your neighbor's.\n",
    "   Did you make the same decisions about how to handle the letter 'y'\n",
    "   (which some people think is a vowel, and some do not)?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Key Points\n",
    "\n",
    "- Use `if condition` to start a conditional statement, `elif condition` to provide additional tests, and `else` to provide a default.\n",
    "- The bodies of the branches of conditional statements must be indented.\n",
    "- Use `==` to test for equality.\n",
    "- `X and Y` is only true if both X and Y are true.\n",
    "- `X or Y` is true if either X or Y, or both, are true.\n",
    "- Zero, the empty string, and the empty list are considered false; all other numbers, strings, and lists are considered true.\n",
    "- Nest loops to operate on multi-dimensional data.\n",
    "- Put code whose parameters change frequently in a function, then call it with different parameter values to customize its behavior.\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
