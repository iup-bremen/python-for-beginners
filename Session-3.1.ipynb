{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Session 3.1: Errors and Exceptions\n",
    "\n",
    "Source: https://swcarpentry.github.io/python-novice-inflammation/07-errors/\n",
    "        \n",
    "Teaching: **30 min**, Exercises: **0 min**\n",
    "\n",
    "### Questions\n",
    "- How does Python report errors?\n",
    "- How can I handle errors in Python programs?\n",
    "\n",
    "### Objectives\n",
    "- To be able to read a traceback, and determine where the error took place and what type it is.\n",
    "- To be able to describe the types of situations in which syntax errors, indentation errors, name errors, index errors, and missing file errors occur."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Every programmer encounters errors,\n",
    "both those who are just beginning,\n",
    "and those who have been programming for years.\n",
    "Encountering errors and exceptions can be very frustrating at times,\n",
    "and can make coding feel like a hopeless endeavour.\n",
    "However,\n",
    "understanding what the different types of errors are\n",
    "and when you are likely to encounter them can help a lot.\n",
    "Once you know *why* you get certain types of errors,\n",
    "they become much easier to fix.\n",
    "\n",
    "Errors in Python have a very specific form,\n",
    "called a [traceback](https://swcarpentry.github.io/python-novice-inflammation/reference/#traceback).\n",
    "Let's examine one:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# This code has an intentional error. You can type it directly or\n",
    "# use it for reference to understand the error message below.\n",
    "def favorite_ice_cream():\n",
    "    ice_creams = [\n",
    "        \"chocolate\",\n",
    "        \"vanilla\",\n",
    "        \"strawberry\"\n",
    "    ]\n",
    "    print(ice_creams[3])\n",
    "\n",
    "favorite_ice_cream()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This particular traceback has two levels.\n",
    "You can determine the number of levels by looking for the number of arrows on the left hand side.\n",
    "In this case:\n",
    "\n",
    "1.  The first shows code from the cell above,\n",
    "    with an arrow pointing to Line 8 (which is `favorite_ice_cream()`).\n",
    "\n",
    "2.  The second shows some code in the function `favorite_ice_cream`,\n",
    "    with an arrow pointing to Line 6 (which is `print(ice_creams[3])`).\n",
    "\n",
    "The last level is the actual place where the error occurred.\n",
    "The other level(s) show what function the program executed to get to the next level down.\n",
    "So, in this case, the program first performed a [function call]({{ page.root }}/reference/#function-call) to the function `favorite_ice_cream`.\n",
    "Inside this function,\n",
    "the program encountered an error on Line 6, when it tried to run the code `print(ice_creams[3])`.\n",
    "\n",
    "> ## Long tracebacks\n",
    "> Sometimes, you might see a traceback that is very long -- sometimes they might even be 20 levels deep!\n",
    "> This can make it seem like something horrible happened,\n",
    "> but really it just means that your program called many functions before it ran into the error.\n",
    "> Most of the time,\n",
    "> you can just pay attention to the bottom-most level,\n",
    "> which is the actual place where the error occurred.\n",
    "\n",
    "So what error did the program actually encounter?\n",
    "In the last line of the traceback,\n",
    "Python helpfully tells us the category or type of error (in this case, it is an `IndexError`)\n",
    "and a more detailed error message (in this case, it says \"list index out of range\").\n",
    "\n",
    "If you encounter an error and don't know what it means,\n",
    "it is still important to read the traceback closely.\n",
    "That way,\n",
    "if you fix the error,\n",
    "but encounter a new one,\n",
    "you can tell that the error changed.\n",
    "Additionally,\n",
    "sometimes just knowing *where* the error occurred is enough to fix it,\n",
    "even if you don't entirely understand the message.\n",
    "\n",
    "If you do encounter an error you don't recognize,\n",
    "try looking at the [official documentation on errors](http://docs.python.org/3/library/exceptions.html).\n",
    "However,\n",
    "note that you may not always be able to find the error there,\n",
    "as it is possible to create custom errors.\n",
    "In that case,\n",
    "hopefully the custom error message is informative enough to help you figure out what went wrong."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Syntax Errors\n",
    "\n",
    "When you forget a colon at the end of a line,\n",
    "accidentally add one space too many when indenting under an `if` statement,\n",
    "or forget a parenthesis,\n",
    "you will encounter a [syntax error](https://swcarpentry.github.io/python-novice-inflammation/reference/#syntax-error).\n",
    "This means that Python couldn't figure out how to read your program.\n",
    "This is similar to forgetting punctuation in English:\n",
    "for example,\n",
    "this text is difficult to read there is no punctuation there is also no capitalization\n",
    "why is this hard because you have to figure out where each sentence ends\n",
    "you also have to figure out where each sentence begins\n",
    "to some extent it might be ambiguous if there should be a sentence break or not\n",
    "\n",
    "People can typically figure out what is meant by text with no punctuation,\n",
    "but people are much smarter than computers.\n",
    "If Python doesn't know how to read the program,\n",
    "it will just give up and inform you with an error.\n",
    "For example:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "def some_function()\n",
    "    msg = \"hello, world!\"\n",
    "    print(msg)\n",
    "     return msg"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here, Python tells us that there is a `SyntaxError` on line 1,\n",
    "and even puts a little arrow in the place where there is an issue.\n",
    "In this case the problem is that the function definition is missing a colon at the end.\n",
    "\n",
    "Actually, the function above has *two* issues with syntax.\n",
    "If we fix the problem with the colon,\n",
    "we see that there is *also* an `IndentationError`,\n",
    "which means that the lines in the function definition do not all have the same indentation:\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "def some_function():\n",
    "    msg = \"hello, world!\"\n",
    "    print(msg)\n",
    "     return msg"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Both `SyntaxError` and `IndentationError` indicate a problem with the syntax of your program,\n",
    "but an `IndentationError` is more specific:\n",
    "it *always* means that there is a problem with how your code is indented."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> ## Tabs and Spaces\n",
    ">\n",
    "> A quick note on indentation errors:\n",
    "> they can sometimes be insidious,\n",
    "> especially if you are mixing spaces and tabs.\n",
    "> Because they are both [whitespace](https://swcarpentry.github.io/python-novice-inflammation/reference/#whitespace),\n",
    "> it is difficult to visually tell the difference.\n",
    "> The Jupyter notebook actually gives us a bit of a hint,\n",
    "> but not all Python editors will do that.\n",
    "> In the following example,\n",
    "> the first two lines are using a tab for indentation,\n",
    "> while the third line uses four spaces:\n",
    ">\n",
    "> ~~~\n",
    "> def some_function():\n",
    "> \tmsg = \"hello, world!\"\n",
    "> \tprint(msg)\n",
    ">     return msg\n",
    "> ~~~\n",
    ">\n",
    "> Therefore, you would get the following error:\n",
    ">\n",
    "> ~~~\n",
    ">   File \"<ipython-input-5-653b36fbcd41>\", line 4\n",
    ">     return msg\n",
    ">               ^\n",
    "> IndentationError: unindent does not match any outer indentation level\n",
    "> ~~~\n",
    ">\n",
    ">\n",
    "> By default, one tab is equivalent to eight spaces,\n",
    "> so the only way to mix tabs and spaces is to make it look like this.\n",
    "> In general, it is better to just never use tabs and always use spaces,\n",
    "> because it can make things very confusing."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Variable Name Errors\n",
    "\n",
    "Another very common type of error is called a `NameError`,\n",
    "and occurs when you try to use a variable that does not exist.\n",
    "For example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print(a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Variable name errors come with some of the most informative error messages,\n",
    "which are usually of the form \"name 'the_variable_name' is not defined\".\n",
    "\n",
    "Why does this error message occur?\n",
    "That's harder question to answer,\n",
    "because it depends on what your code is supposed to do.\n",
    "However,\n",
    "there are a few very common reasons why you might have an undefined variable.\n",
    "The first is that you meant to use a [string](https://swcarpentry.github.io/python-novice-inflammation/reference/#string), but forgot to put quotes around it:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print(hello)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The second is that you just forgot to create the variable before using it.\n",
    "In the following example,\n",
    "`count` should have been defined (e.g., with `count = 0`) before the for loop:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "for number in range(10):\n",
    "    count = count + number\n",
    "print(\"The count is:\", count)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, the third possibility is that you made a typo when you were writing your code.\n",
    "Let's say we fixed the error above by adding the line `Count = 0` before the for loop.\n",
    "Frustratingly, this actually does not fix the error.\n",
    "Remember that variables are [case-sensitive](https://swcarpentry.github.io/python-novice-inflammation/reference/#case-sensitive),\n",
    "so the variable `count` is different from `Count`. We still get the same error, because we still have not defined `count`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "Count = 0\n",
    "for number in range(10):\n",
    "    count = count + number\n",
    "print(\"The count is:\", count)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Index Errors\n",
    "\n",
    "Next up are errors having to do with containers (like lists and strings) and the items within them.\n",
    "If you try to access an item in a list or a string that does not exist,\n",
    "then you will get an error.\n",
    "This makes sense:\n",
    "if you asked someone what day they would like to get coffee,\n",
    "and they answered \"caturday\",\n",
    "you might be a bit annoyed.\n",
    "Python gets similarly annoyed if you try to ask it for an item that doesn't exist:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "letters = ['a', 'b', 'c']\n",
    "print(\"Letter #1 is\", letters[0])\n",
    "print(\"Letter #2 is\", letters[1])\n",
    "print(\"Letter #3 is\", letters[2])\n",
    "print(\"Letter #4 is\", letters[3])\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here,\n",
    "Python is telling us that there is an `IndexError` in our code,\n",
    "meaning we tried to access a list index that did not exist.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## File Errors\n",
    "\n",
    "The last type of error we'll cover today\n",
    "are those associated with reading and writing files: `FileNotFoundError`.\n",
    "If you try to read a file that does not exist,\n",
    "you will receive a `FileNotFoundError` telling you so.\n",
    "If you attempt to write to a file that was opened read-only, Python 3\n",
    "returns an `UnsupportedOperationError`.\n",
    "More generally, problems with input and output manifest as\n",
    "`IOError`s or `OSError`s, depending on the version of Python you use."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "file_handle = open('myfile.txt', 'r')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One reason for receiving this error is that you specified an incorrect path to the file.\n",
    "For example,\n",
    "if I am currently in a folder called `myproject`,\n",
    "and I have a file in `myproject/writing/myfile.txt`,\n",
    "but I try to just open `myfile.txt`,\n",
    "this will fail.\n",
    "The correct path would be `writing/myfile.txt`.\n",
    "It is also possible (like with `NameError`) that you just made a typo.\n",
    "\n",
    "A related issue can occur if you use the \"read\" flag instead of the \"write\" flag.\n",
    "Python will not give you an error if you try to open a file for writing when the file does not exist.\n",
    "However,\n",
    "if you meant to open a file for reading,\n",
    "but accidentally opened it for writing,\n",
    "and then try to read from it,\n",
    "you will get an `UnsupportedOperation` error\n",
    "telling you that the file was not opened for reading:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "file_handle = open('myfile.txt', 'w')\n",
    "file_handle.read()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These are the most common errors with files,\n",
    "though many others exist.\n",
    "If you get an error that you've never seen before,\n",
    "searching the Internet for that error type\n",
    "often reveals common reasons why you might get that error.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise: Reading Error Messages\n",
    "\n",
    "Read this python code and the resulting traceback, and answer the following questions:\n",
    "\n",
    "1.  How many levels does the traceback have?\n",
    "2.  What is the function name where the error occurred?\n",
    "3.  On which line number in this function did the error occurr?\n",
    "4.  What is the type of error?\n",
    "5.  What is the error message?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# This code has an intentional error. Do not type it directly;\n",
    "# use it for reference to understand the error message below.\n",
    "def print_message(day):\n",
    "    messages = {\n",
    "        \"monday\": \"Hello, world!\",\n",
    "        \"tuesday\": \"Today is tuesday!\",\n",
    "        \"wednesday\": \"It is the middle of the week.\",\n",
    "        \"thursday\": \"Today is Donnerstag in German!\",\n",
    "        \"friday\": \"Last day of the week!\",\n",
    "        \"saturday\": \"Hooray for the weekend!\",\n",
    "        \"sunday\": \"Aw, the weekend is almost over.\"\n",
    "    }\n",
    "    print(messages[day])\n",
    "\n",
    "def print_friday_message():\n",
    "    print_message(\"Friday\")\n",
    "\n",
    "print_friday_message()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise: Identifying Syntax Errors\n",
    "\n",
    "1. Read the code below, and (without running it) try to identify what the errors are.\n",
    "2. Run the code, and read the error message. Is it a `SyntaxError` or an `IndentationError`?\n",
    "3. Fix the error.\n",
    "4. Repeat steps 2 and 3, until you have fixed all the errors.\n",
    "\n",
    "~~~\n",
    "def another_function\n",
    "    print(\"Syntax errors are annoying.\")\n",
    "    print(\"But at least python tells us about them!\")\n",
    "   print(\"So they are usually not too hard to fix.\")\n",
    "~~~"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise: Identifying Variable Name Errors\n",
    "\n",
    "1. Read the code below, and (without running it) try to identify what the errors are.\n",
    "2. Run the code, and read the error message.\n",
    "   What type of `NameError` do you think this is?\n",
    "   In other words, is it a string with no quotes,\n",
    "   a misspelled variable,\n",
    "   or a variable that should have been defined but was not?\n",
    "3. Fix the error.\n",
    "4. Repeat steps 2 and 3, until you have fixed all the errors.\n",
    "\n",
    "~~~\n",
    "for number in range(10):\n",
    "    # use a if the number is a multiple of 3, otherwise use b\n",
    "    if (Number % 3) == 0:\n",
    "        message = message + a\n",
    "    else:\n",
    "        message = message + \"b\"\n",
    "print(message)\n",
    "~~~"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise: Identifying Index Errors\n",
    "\n",
    "1. Read the code below, and (without running it) try to identify what the errors are.\n",
    "2. Run the code, and read the error message. What type of error is it?\n",
    "3. Fix the error.\n",
    "\n",
    "~~~\n",
    "seasons = ['Spring', 'Summer', 'Fall', 'Winter']\n",
    "print('My favorite season is ', seasons[4])\n",
    "~~~"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Key Points\n",
    "\n",
    "- Tracebacks can look intimidating, but they give us a lot of useful information about what went wrong in our program, including where the error occurred and what type of error it was.\n",
    "- An error having to do with the 'grammar' or syntax of the program is called a `SyntaxError`. If the issue has to do with how the code is indented, then it will be called an `IndentationError`.\n",
    "- A `NameError` will occur if you use a variable that has not been defined, either because you meant to use quotes around a string, you forgot to define the variable, or you just made a typo.\n",
    "- Containers like lists and strings will generate errors if you try to access items in them that do not exist. This type of error is called an `IndexError`.\n",
    "- Trying to read a file that does not exist will give you an `FileNotFoundError`. Trying to read a file that is open for writing, or writing to a file that is open for reading, will give you an `IOError`.\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
