{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import pandas as pd\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline\n",
    "plt.style.use('ggplot')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# How to deal with datetimes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Different types of datetimes\n",
    "There are different concepts of representing time stamps in Python:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The `datetime` module\n",
    "\n",
    "The `datetime` module is Python's standard way of dealing with timestamp objects."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import datetime\n",
    "# year, month, day, hour, minute, second, microsecond\n",
    "d = datetime.datetime(2016, 12, 24, 23, 45, 6, 0)\n",
    "print(d, type(d))\n",
    "\n",
    "# now\n",
    "print(datetime.datetime.now())\n",
    "\n",
    "# from a string\n",
    "print(datetime.datetime.strptime('2016-12-24 23:45:06',\n",
    "                                 '%Y-%m-%d %H:%M:%S'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A `datetime` object can be split into a `date` and a `time` object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print(d.date(), type(d.date()))\n",
    "print(d.time(), type(d.time()))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For arithmetic with `datetime` objects, there is `datetime.timedelta`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "d + datetime.timedelta(hours=1, microseconds=321)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For conversion to strings, there are two different possiblilities:\n",
    "\n",
    "1. `datetime.datetime.strftime`\n",
    "2. `str.format`\n",
    "3. `datetime.datetime.isoformat` for creating an ISO formatted string\n",
    "\n",
    "Documentation on string formating of `datetime` objects is available at https://docs.python.org/3.5/library/datetime.html#strftime-strptime-behavior."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print('using strftime:         ', d.strftime('%Y-%m-%d %H:%M'))\n",
    "print('using isoformat:        ', d.isoformat())\n",
    "print('using string formating:  {:%Y-%m-%d}'.format(d))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exercise\n",
    "Play around a bit with `datetime` objects.  For example, create a `datetime` for yesterday at noon.  Also try converting strings to datetimes and vice versa."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "dy = datetime.datetime.now() + datetime.timedelta(days=-1)\n",
    "dy.hour = 12\n",
    "dy.minute = 0\n",
    "dy.second = 0\n",
    "dy.microsecond = 0"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "dy = datetime.datetime.now() - datetime.timedelta(days=1)\n",
    "datetime.datetime(dy.year, dy.month, dy.day, 12)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "dy = datetime.date.today() - datetime.timedelta(days=1)\n",
    "datetime.datetime(dy.year, dy.month, dy.day, 12)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "dy = datetime.datetime.now() - datetime.timedelta(days=1)\n",
    "datetime.datetime.combine(dy.date(), datetime.time(12))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "dy = datetime.date.today() - datetime.timedelta(days=1)\n",
    "datetime.datetime.combine(dy, datetime.time(12))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### NumPy `datetime64` dtype\n",
    "NumPy arrays can have the data type `datetime64`.  Documentation is available at https://docs.scipy.org/doc/numpy/reference/arrays.datetime.html."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "n = np.datetime64('2016-12-24T23:45:06')\n",
    "print(n, type(n), n.dtype)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These `datetime64` elements can have different *resolution*; supported are resolutions from years to attoseconds."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print(np.datetime64('2005-02-25'),\n",
    "      np.datetime64('2005-02-25').dtype)\n",
    "print(np.datetime64('2005-02'),\n",
    "      np.datetime64('2005-02').dtype)\n",
    "print(np.datetime64('2005-02', 'D'),\n",
    "      np.datetime64('2005-02', 'D').dtype)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For arithmetic with `datetime64` dtypes, there is `numpy.timedelta64`, which supports different time units."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print(n + np.timedelta64(10, 'D'))\n",
    "print(n + np.timedelta64(10, 'h'))\n",
    "print(n + np.timedelta64(10, 'm'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Direct options to convert a `datetime64` to a string are limited.  For more flexibility, you need to convert to `datetime` first (see below)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print('using astype:         ', n.astype(str))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exercise\n",
    "Play around with NumPy's `datetime64`.  Try out different resolutions, and see what happens when you do arithmetic with different resolutions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Pandas `Timestamp`\n",
    "Pandas has a class `pd.Timestamp`, which can in many respects be used like `datetime` objects, just that they support element-wise operation on arrays (or, rather, Pandas objects).  Documentation is available at http://pandas.pydata.org/pandas-docs/stable/timeseries.html."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "p = pd.Timestamp('2016-12-24T23:45:06.0')\n",
    "print(p, type(p))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "p.day, p.month, p.second"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Conversion between the different types"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### from `pd.Timestamp`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print(p.to_datetime(), type(p.to_datetime()))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print(p.to_datetime64(), type(p.to_datetime64()))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### from `np.datetime64`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print(n.astype(datetime.datetime), type(n.astype(datetime.datetime)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# !!! CAUTION !!!\n",
    "print(n.astype(pd.Timestamp), type(n.astype(pd.Timestamp)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# !!! instead, use !!!\n",
    "print(pd.Timestamp(n), type(pd.Timestamp(n)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### from `datetime.datetime`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print(np.datetime64(d), type(np.datetime64(d)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print(pd.Timestamp(d), type(pd.Timestamp(d)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exercise\n",
    "Play around with conversions from one data type to another.  See what happens when you convert NumPy `datetime64` with different resolutions into other data types."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Arrays, lists, ..., of time stamps"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "dts = [datetime.datetime(2016, 12, d) for d in range(1,32)]\n",
    "print(dts)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "nps = np.arange('2016-12-01', '2017-01-01', dtype='datetime64')\n",
    "print(nps, type(nps), nps.dtype)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "pds = pd.DatetimeIndex(start='2016-12-01', end='2016-12-31',\n",
    "                       freq='D')\n",
    "print(pds, type(pds), pds.dtype)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "pd.date_range(start='2016-12-01', end='2016-12-31', freq='D')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The nice thing about the Pandas way of doing dates is that it's possible to access individual components element-wise."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "pds.day, pds.month"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "np.datetime64(dts)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "np.array(dts, dtype='datetime64')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`Timestamp` is the data type for a single time stamp object, comparable to `datetime.datetime`.  For creating a Pandas array (or, `DatetimeIndex` from a list of dates, use `pd.DatetimeIndex`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "pd.DatetimeIndex(dts)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "pd.DatetimeIndex(nps)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exercise\n",
    "Explore the different ways to create evenly-spaced datetime arrays in NumPy and Pandas.  Play around with different time resolutions and different intervals."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Other useful tools"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Converting numbers to datetime\n",
    "The NetCDF4 module in Python has very useful functions `num2date` and `date2num`.  These functions \"understand\" human-language descriptions, like *days since 2016-01-01*, and allow for easy, array-wise conversion of numbers to datetime objects and vice versa.  Documentation is available at https://unidata.github.io/netcdf4-python/."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from netCDF4 import date2num, num2date\n",
    "n2d = num2date(24, 'days since 2016-11-30')\n",
    "print(n2d, type(n2d))\n",
    "d2n = date2num(datetime.datetime.now(), 'days since 2016-11-30')\n",
    "print(d2n, type(d2n))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This also works with lists and arrays"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print(num2date([24, 24.5],\n",
    "               'days since 2016-11-30'))\n",
    "print()\n",
    "print(num2date(np.linspace(24, 25, 3),\n",
    "               'days since 2016-11-30'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exercise\n",
    "How could you use the `num2date` function for creating the time stamps in the NO2 data files?  (Remember that the first column contains the day-of-year-1993, i.e., the number of days which passed since the beginning of 1993, with 1.0 being 1993-01-01T00:00:00)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "no2 = pd.read_csv('../data/no2-athens/130623TS.VisNO2A',\n",
    "                comment='*',\n",
    "                delim_whitespace=True,\n",
    "                header=None # don't care about proper column names\n",
    "                )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Using not-well-defined timedeltas\n",
    "Some timedelta units, e.g., seconds, days, minutes, are uniquely defined periods of time.  However, for longer periods, the common units are not uniquely defined any more.  For example a month can have different number of days, and years can have different number of days (gap years), and sometimes there are gap seconds and more esoteric things."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "datetime.datetime.now() + datetime.timedelta(months=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To deal with the easier of these (i.e., months and years), there is `dateutil.relativedelta`.  Documentation is available at https://dateutil.readthedocs.io/en/stable/relativedelta.html."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from dateutil.relativedelta import relativedelta\n",
    "datetime.datetime.now() + relativedelta(months=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also combine several units"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "datetime.datetime.now() + relativedelta(months=1, weeks=2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, you can set a certain point in time after a given period has passed (starting at the beginning of today)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# 10:00:00 after one month has passed from the beginning of today\n",
    "datetime.datetime.now() + relativedelta(months=1,\n",
    "                                        hour=10,\n",
    "                                        minute=0, second=0,\n",
    "                                        microsecond=0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Beware of singular / plural language\n",
    "datetime.datetime.now() + relativedelta(months=1,\n",
    "                                        hours=10,\n",
    "                                        minute=0, second=0,\n",
    "                                        microsecond=0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exercise\n",
    "Create a datetime object for 12:00 of today one month ago.  What different ways to do this can you think about?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Time periods instead of points in time\n",
    "Sometimes, it can be useful to work with *time periods* instead of *points in time*."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In principle, `datetime.date` represents a *day*, so it is a time period.  However, the `datetime.date` object is really only a date, it does not \"know\" the notion of *duration*.\n",
    "\n",
    "Similar is true for `np.datetime64`, which is a point in time with a given resolution, without notion of *start* and *end*.\n",
    "\n",
    "In Pandas, there is the `pd.Period` object"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "dec = pd.Period('2016-12', 'M')\n",
    "print(dec, type(dec))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "dec.start_time, dec.end_time"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "dec.start_time <= datetime.datetime(2016, 12, 24) <= dec.end_time"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Parsing strings for date information\n",
    "In addition to the above mentioned methods to create time stamps from strings (`datetime.datetime.strptime()` and the direct usage of strings in `np.datetime64` and `pd.Timestamp` initialization), there is the module `dateutil.parser` which can sometimes be useful.  Documentation is available at https://dateutil.readthedocs.io/en/stable/parser.html."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from dateutil.parser import parse as parse_date\n",
    "pd1 = parse_date('Thu Sep 25 10:36:28 2003')\n",
    "pd2 = parse_date('2003-09-25 10:36:28')\n",
    "pd3 = parse_date('25.09.2003 10:36:28')\n",
    "pd4 = parse_date('09/25/2003 10:36:28')\n",
    "pd5 = parse_date('25 Sep 2003 10:36:28')\n",
    "assert pd1 == pd2 == pd3 == pd4 == pd5\n",
    "print(pd1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# !!! CAUTION !!!\n",
    "pd1 = parse_date('10 Sep 2003 10:36:28')\n",
    "pd2 = parse_date('09/10/2003 10:36:28')\n",
    "pd3 = parse_date('10.09.2003 10:36:28')\n",
    "pd4 = parse_date('13.09.2003 10:36:28')\n",
    "print(pd3)\n",
    "print(pd4)\n",
    "assert pd1 == pd2 == pd3"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
