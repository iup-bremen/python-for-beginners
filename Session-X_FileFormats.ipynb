{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Common file formats and how to deal with them\n",
    "\n",
    "(c) Andreas.Hilboll@uni-bremen.de, 28 Feb 2017\n",
    "\n",
    "This document briefly lists how to open a number of common scientific data formats.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Text-based data (ASCII, .csv, ...)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "\n",
    "# load data\n",
    "data = pd.read_csv('data/140101_171231_Berlin_FUB_lev20.csv', header=4,\n",
    "                   dayfirst=True, parse_dates=[[0,1]], index_col=0)\n",
    "\n",
    "# rename index column\n",
    "data.index.name = 'timestamp'\n",
    "data.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## NetCDF data\n",
    "\n",
    "You can download a test dataset from [here](download test data from https://github.com/pydata/xarray-data/blob/master/air_temperature.nc?raw=true).\n",
    "\n",
    "The basic way to deal with NetCDF files is the `netcdf4` library:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import netCDF4\n",
    "\n",
    "nc = netCDF4.Dataset('data/air_temperature.nc', 'r')\n",
    "print(nc.variables.keys())\n",
    "\n",
    "lon = nc.variables['lon'][:]\n",
    "lat = nc.variables['lat'][:]\n",
    "data = nc.variables['air'][:]\n",
    "\n",
    "nc.close()    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A more useful interface to NetCDF files is *xarray*, which in many respects is similar to *pandas*:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import xarray as xr\n",
    "\n",
    "data = xr.open_dataset('data/air_temperature.nc')\n",
    "data.info()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## XLS files (Excel)\n",
    "\n",
    "Download a test data file from [here](https://www.eia.gov/electricity/data/state/emission_annual.xls).\n",
    "\n",
    "`.xls` files can be read by *pandas*, with the `read_excel` funtion:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "pd.read_excel('data/emission_annual.xls')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## TDMS\n",
    "\n",
    "*TDMS* files can be read by the [npTDMS](https://nptdms.readthedocs.io/en/latest/index.html) package, which can be installed by the command (to be executed in your chosen anaconda environment)\n",
    "\n",
    "    pip install nptdms"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from nptdms import TdmsFile\n",
    "tdms_file = TdmsFile('data/170111_172420_raw_PXI4.tdms')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Which groups are in the file?\n",
    "print('Groups in the file:', tdms_file.groups())\n",
    "print()\n",
    "\n",
    "# convert to pandas DataFrame\n",
    "df = tdms_file.as_dataframe()\n",
    "df.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "\n",
    "# plot the 10th trace in the file\n",
    "trace10 = df.iloc[:, 9]\n",
    "\n",
    "trace10.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## HDF5\n",
    "\n",
    "You can download a test data file from [here](http://temis.nl/airpollution/no2col/data/omi/data_v2/2017/omi_no2_he5_20170228.tar) (you'll need to extract the `.tar` file first).\n",
    "\n",
    "The easiest way to deal with HDF5 files is with the [h5py](http://docs.h5py.org/en/latest/quick.html) library.  However, this is a rather low-level library, so it helps to know the structure of your files.\n",
    "\n",
    "On Linux, you can run the following command to look at the structure of a data file:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "!h5dump -H data/OMI-Aura_L2-OMDOMINO_2017m0228t0051-o67145_v883-2017m0228t042020.he5"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import h5py\n",
    "data = h5py.File('data/OMI-Aura_L2-OMDOMINO_2017m0228t0051-o67145_v883-2017m0228t042020.he5', 'r')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In order to look at the file structure from within Python, you'll need to create `list`s of the `keys()` argument, i.e.,"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "list(data.keys())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "list(data['HDFEOS']['SWATHS']['DominoNO2']['Data Fields'].keys())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then you can simply access the data as"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "data_array = data['HDFEOS']['SWATHS']['DominoNO2']['Data Fields']['SlantColumnAmountNO2'][:]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Pickle (pkl)\n",
    "\n",
    "Any Python object can be saved on disk using the [pickle](https://docs.python.org/3/library/pickle.html) module:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "my_list = ['a', 1, 34.3, [12, 'b']]\n",
    "print(my_list)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import pickle\n",
    "with open('data/pickletest.pkl.gz', 'wb') as fh:\n",
    "    pickle.dump(my_list, fh)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "!ls -lh data/pickletest.pkl.gz"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print(my_new_list)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "with open('data/pickletest.pkl.gz', 'rb') as fh:\n",
    "    my_new_list = pickle.load(fh)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print(my_new_list)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Matlab\n",
    "\n",
    "You can download a test data file from [here](https://github.com/scipy/scipy/blob/master/scipy/io/matlab/tests/data/testmatrix_4.2c_SOL2.mat?raw=true).\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import scipy.io.matlab\n",
    "data = scipy.io.matlab.loadmat('data/testmatrix_4.2c_SOL2.mat')\n",
    "print(data)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
