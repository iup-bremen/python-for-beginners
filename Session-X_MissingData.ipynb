{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import pandas as pd\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline\n",
    "plt.style.use('ggplot')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# How to deal with missing data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Outlier detection\n",
    "\n",
    "First, let us create a data set of random dummy data to work with:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "index = pd.DatetimeIndex(start='2016-01-01',\n",
    "                         end='2016-12-31',\n",
    "                         freq='1min')\n",
    "print('This index has', index.size, 'elements')\n",
    "np.random.seed(2)\n",
    "testdata = pd.Series(np.random.randn(index.size),\n",
    "                     index=index)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using the `testdata` from above, let's suppose that we know that data points with an absolute value $>4.35$ are outliers, and we want to see where these values occur.  We can create a boolean mask to select only these values"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "mask = testdata.abs() > 4.35\n",
    "print('There are', mask.sum(), 'data points larger than 4.35')\n",
    "mask.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can look at those data points only"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "testdata[mask]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So now we know the times when our data is strange."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, if you want to exclude these data points from further analysis, you can assign them the no-data value `np.nan`. Pandas automatically \"does the right thing\" with missing data; see http://pandas.pydata.org/pandas-docs/stable/missing_data.html for details."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "testdata[mask] = np.nan\n",
    "testdata[mask]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "testdata.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exercise\n",
    "Find out what the `isnull()` and `notnull()` methods of the `testdata` do."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exercise\n",
    "Try out how Pandas handles missing data in computations.  For example, look at the `.sum()`, `.mean()`, ..., of the series.  Compare to the `.sum()` and `.mean()` of the underlying NumPy array `testdata.values`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Filling missing data points\n",
    "Sometimes, you might want to fill missing data values.  The easiest way to do this is the `fillna()` method:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "filldata = testdata.copy()\n",
    "print(filldata[mask])\n",
    "filldata.fillna(123.0)[mask]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Interpolation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In other situations, you might want to infer the data values at the missing points from the surrounding data.  This is called *interpolation*.  Pandas objects have this functionality built-in, using SciPy."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "np.random.seed(2)\n",
    "ser = pd.Series(np.arange(1, 10.1, .25)**2 +\n",
    "                np.random.randn(37))\n",
    "bad = np.array([4, 13, 14, 15, 16, 17, 18, 20, 29])\n",
    "ser[bad] = np.nan\n",
    "\n",
    "# try different methods for interplation\n",
    "methods = ['linear', 'quadratic', 'cubic']\n",
    "df = pd.DataFrame()\n",
    "for m in methods:\n",
    "    df[m] = ser.interpolate(method=m)\n",
    "\n",
    "# or, using dictionary comprehension\n",
    "# df = pd.DataFrame({m: ser.interpolate(method=m) for m in methods})"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()\n",
    "ser.plot(style='.', label='original data',\n",
    "         markersize=10, ax=ax)\n",
    "df.plot(ax=ax)\n",
    "ax.legend(loc='upper left')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We'll come back to interpolation in a later lecture."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Calculations with missing data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Often, when you have data with missing values, it is enough to simply remove the invalid data points using `notnull` as an index"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "valid_data = ser[ser.notnull()]\n",
    "print(valid_data.head())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And you can always just extract the underlying NumPy array."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "ser.values"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Pandas has the basic statistical functions built in, to deal with missing data conveniently.  If you need additional functionality, you can always extract the underlying NumPy array."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, then you need to take care of the `np.nan` values manually."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "ser.mean()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "ser.values.mean()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "np.nanmean(ser.values)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "np.nan == np.nan"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "~ np.isnan(ser.values)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There is the `masked_array` class in NumPy to deal with these situations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "masked_ser = np.ma.masked_invalid(ser.values)\n",
    "print(type(masked_ser))\n",
    "print(masked_ser)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `mask` attribute of the `masked_array` is identical to the `isnull()` of the original Pandas object:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "masked_ser.mask == ser.isnull()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And `masked_array` objects have the same built-in functions as regular NumPy arrays:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print(masked_ser.mean())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This becomes really interesting when using advanced statistics, using the `scipy.stats.mstats` module (later)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exercise\n",
    "Using `np.ma.masked_where`, create a masked array from `ser` where every value of `ser` which is larger than 40 and smaller than 50 is masked.  Calculate the mean.\n",
    "\n",
    "How could you do this directly with the `pd.Series` object, without converting to `masked_array` first?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
