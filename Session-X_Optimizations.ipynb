{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import datetime\n",
    "\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import matplotlib as mpl\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Speeding up calculations\n",
    "\n",
    "(c) Andreas.Hilboll@uni-bremen.de, Feb 2017\n",
    "\n",
    "## with Numba\n",
    "\n",
    "Source: http://numba.pydata.org/numba-doc/dev/user/examples.html\n",
    "\n",
    "[Numba](http://numba.pydata.org/) is a *just-in-time-compiler* for pure Python code.  It takes a Python function, and compiles optimized machine code from it before executing the function - all without any user intervention.\n",
    "\n",
    "To demonstrate the potential, here is a simple example calculating a Mandelbrot set:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "def mandel(x, y, max_iters):\n",
    "    \"\"\"\n",
    "    Given the real and imaginary parts of a complex number,\n",
    "    determine if it is a candidate for membership in the Mandelbrot\n",
    "    set given a fixed number of iterations.\n",
    "    \"\"\"\n",
    "    i = 0\n",
    "    c = complex(x,y)\n",
    "    z = 0.0j\n",
    "    for i in range(max_iters):\n",
    "        z = z*z + c\n",
    "        if (z.real*z.real + z.imag*z.imag) >= 4:\n",
    "            return i\n",
    "\n",
    "    return 255\n",
    "\n",
    "def create_fractal(min_x, max_x, min_y, max_y, image, iters):\n",
    "    height = image.shape[0]\n",
    "    width = image.shape[1]\n",
    "\n",
    "    pixel_size_x = (max_x - min_x) / width\n",
    "    pixel_size_y = (max_y - min_y) / height\n",
    "    for x in range(width):\n",
    "        real = min_x + x * pixel_size_x\n",
    "        for y in range(height):\n",
    "            imag = min_y + y * pixel_size_y\n",
    "            color = mandel(real, imag, iters)\n",
    "            image[y, x] = color\n",
    "\n",
    "    return image\n",
    "\n",
    "image = np.zeros((500 * 2, 750 * 2), dtype=np.uint8)\n",
    "%time create_fractal(-2.0, 1.0, -1.0, 1.0, image, 20)\n",
    "plt.imshow(image)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using Numba for this calculation is extremely easy.\n",
    "\n",
    "Of course, you need to install it:\n",
    "\n",
    "    conda install numba\n",
    "    \n",
    "Then, you just need to `from numba import jit`, and then add the `@jit` decorator to the function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from numba import jit\n",
    "\n",
    "@jit\n",
    "def mandel_numba(x, y, max_iters):\n",
    "    \"\"\"\n",
    "    Given the real and imaginary parts of a complex number,\n",
    "    determine if it is a candidate for membership in the Mandelbrot\n",
    "    set given a fixed number of iterations.\n",
    "    \"\"\"\n",
    "    i = 0\n",
    "    c = complex(x,y)\n",
    "    z = 0.0j\n",
    "    for i in range(max_iters):\n",
    "        z = z*z + c\n",
    "        if (z.real*z.real + z.imag*z.imag) >= 4:\n",
    "            return i\n",
    "\n",
    "    return 255\n",
    "\n",
    "@jit\n",
    "def create_fractal_numba(min_x, max_x, min_y, max_y, image, iters):\n",
    "    height = image.shape[0]\n",
    "    width = image.shape[1]\n",
    "\n",
    "    pixel_size_x = (max_x - min_x) / width\n",
    "    pixel_size_y = (max_y - min_y) / height\n",
    "    for x in range(width):\n",
    "        real = min_x + x * pixel_size_x\n",
    "        for y in range(height):\n",
    "            imag = min_y + y * pixel_size_y\n",
    "            color = mandel_numba(real, imag, iters)\n",
    "            image[y, x] = color\n",
    "\n",
    "    return image\n",
    "\n",
    "image = np.zeros((500 * 2, 750 * 2), dtype=np.uint8)\n",
    "%time create_fractal_numba(-2.0, 1.0, -1.0, 1.0, image, 20)\n",
    "plt.imshow(image)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These three extra lines of code improve the runtime of the calculation by a factor of 20.  More examples are available at the [Numba documentation](http://numba.pydata.org/numba-doc/0.30.1/index.html)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Optimizing with C-Code (Cython)\n",
    "\n",
    "Using [Cython](http://cython.org/), it is possible to \n",
    "\n",
    "1. gain speed benefits by writing Python-like code which is automatically compiled in *C*\n",
    "2. write interfaces to *C* libraries (e.g., drivers of some instruments).\n",
    "\n",
    "You probably have to install *Cython* by running the following command:\n",
    "\n",
    "    conda install cython"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def primes_py(kmax):\n",
    "    result = []\n",
    "    p = np.empty(kmax)\n",
    "    if kmax > 1000:\n",
    "        kmax = 1000\n",
    "    k = 0\n",
    "    n = 2\n",
    "    while k < kmax:\n",
    "        i = 0\n",
    "        while i < k and n % p[i] != 0:\n",
    "            i = i + 1\n",
    "        if i == k:\n",
    "            p[k] = n\n",
    "            k = k + 1\n",
    "            result.append(n)\n",
    "        n = n + 1\n",
    "    return result"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using the `%timeit` magic, we can measure the time a command takes to complete:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "%timeit primes_py(1000)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is very easy to use Cython inside a notebook"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "%load_ext Cython"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "%%cython\n",
    "\n",
    "def primes_c(int kmax):\n",
    "    cdef int n, k, i\n",
    "    cdef int p[1000]\n",
    "    result = []\n",
    "    if kmax > 1000:\n",
    "        kmax = 1000\n",
    "    k = 0\n",
    "    n = 2\n",
    "    while k < kmax:\n",
    "        i = 0\n",
    "        while i < k and n % p[i] != 0:\n",
    "            i = i + 1\n",
    "        if i == k:\n",
    "            p[k] = n\n",
    "            k = k + 1\n",
    "            result.append(n)\n",
    "        n = n + 1\n",
    "    return result"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "%timeit primes_c(1000)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Caching of results\n",
    "\n",
    "Another way of speeding up your data analysis could be *caching* of results.  [Joblib](https://pythonhosted.org/joblib/) provides a very simple decorator to automatically store the results of a function call on your hard drive, depending on its inputs.  The function will only be computed one time for each set of inputs.\n",
    "\n",
    "You will probably have to install the library by running the following command:\n",
    "\n",
    "    conda install joblib"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from joblib import Memory\n",
    "\n",
    "memory = Memory(cachedir='./joblib_cache', verbose=False)\n",
    "\n",
    "@memory.cache\n",
    "def g(x):\n",
    "    print('A long-running calculation, with parameter %s' % x)\n",
    "    return np.hamming(x)\n",
    "\n",
    "@memory.cache\n",
    "def h(x):\n",
    "    print('A second long-running calculation, using g(x)')\n",
    "    return np.vander(x)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "!rm -rf joblib_cache/"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print(\"Calling g(3) for the first time:\")\n",
    "a = g(3)\n",
    "print(a)\n",
    "print()\n",
    "print(\"Calling g(3) for the second time:\")\n",
    "print(g(3))\n",
    "print()\n",
    "print(\"Calling g(4) for the first time:\")\n",
    "print(g(4))\n",
    "print()\n",
    "print(\"Calling g(4) for the second time:\")\n",
    "print(g(4))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "!ls joblib_cache/joblib/__main__--home2-hilboll-doc-teaching-2016_DataAnalysis-pdap-2016-lectures-__ipython-input__/g/"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print()\n",
    "print(\"Calling h(a) for the first time:\")\n",
    "b = h(a)\n",
    "print(b)\n",
    "print()\n",
    "print(\"Calling h(a) for the second time:\")\n",
    "b2 = h(a)\n",
    "b == b2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Links\n",
    "\n",
    "- [Cython](http://cython.readthedocs.io/en/latest/src/tutorial/cython_tutorial.html)\n",
    "- Wrapping Fortran code with [f2py](https://docs.scipy.org/doc/numpy-dev/f2py/)\n",
    "- https://www.ibm.com/developerworks/community/blogs/jfp/entry/How_To_Compute_Mandelbrodt_Set_Quickly?lang=en\n",
    "- https://jakevdp.github.io/blog/2015/02/24/optimizing-python-with-numpy-and-numba/\n",
    "- https://lectures.quantecon.org/py/need_for_speed.html"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
