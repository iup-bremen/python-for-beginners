{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib as mpl\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline\n",
    "plt.style.use('ggplot')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Linear regression: Fitting a straight line"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's assume we have measured data $y$ which follows $y = m \\cdot x + b$, but our measurements are affected by (normally distributed) noise:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "np.random.seed(321)\n",
    "m_true = 1.6\n",
    "b_true = -2.\n",
    "noise = np.random.randn(21)\n",
    "x = np.linspace(0., 10., 21)\n",
    "y_true = m_true * x + b_true\n",
    "y_noisy = y_true + noise"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "plt.plot(x, y_true, 'b.', ms=10, label=('true'))\n",
    "plt.plot(x, y_noisy, 'rv', ms=8, label=('noisy'))\n",
    "plt.legend(loc=0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The parameters $m$ and $b$ can be determined by *linear regression*.  The simplest way to calculate these parameters is by using the `numpy.polyfit` function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "m, b = np.polyfit(x, y_noisy, 1)\n",
    "print('Slope and intercept using np.polyfit: ', m, b)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "plt.plot(x, y_true, 'b.', ms=10, label=('true'))\n",
    "plt.plot(x, y_noisy, 'rv', ms=8, label=('noisy'))\n",
    "plt.plot(x, m * x + b, 'c-', lw=2, label=('fitted'))\n",
    "plt.legend(loc=0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If information about the fit error is needed, we have to use more powerful fit functions, for example, `scipy.stats.linregress`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from scipy.stats import linregress"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exercise"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "1. Use the `scipy.stats.linregress` function to calculate the parameters $m$ and $b$.\n",
    "2. Verify that the results are identical to those from `numpy.polyfit`.\n",
    "3. What are the other outputs of the `linregress` function?  What do they mean?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For more sophisticated statstics, there is the [statsmodels](http://statsmodels.sourceforge.net/) package.  It works with *pandas* objects instead of *numpy* arrays."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "from statsmodels.formula.api import ols"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "data = pd.DataFrame({'x': x, 'y': y_noisy})"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print(data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The interdependency of the variables in the `DataFrame` can be expressed in *formula notation*, which you might know from *R*.  A simple linear dependency $f(x) = m \\cdot x + b$ is written as `y ~ x` (the intercept is automatically added)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "lm = ols(formula='y ~ x', data=data).fit()\n",
    "print(lm.params)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*statsmodels* calculates many statistics for the data set, which can be printed like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "lm.summary()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exercise\n",
    "1. Retrieve the estimated parameters from the model above. **Hint:** use tab-completion to find the relevent attribute.\n",
    "2. Plot the data points and the fitted line (similarly to above). **Hint:** Use the `lm.predict` function to create the $y$ values, using a `np.linspace` as $x$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, for a quick visual statistical analysis, there is the [Seaborn](http://seaborn.pydata.org/) package:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import seaborn as sns"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "sns.lmplot(y='y', x='x', data=data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Note:** Seaborn changes the default of matplotlib figures to achieve a more “modern”, “excel-like” look. It does that upon import.  Fore more details, check out the [relevant parts of the Seaborn documentation](http://seaborn.pydata.org/tutorial/aesthetics.html)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Outlier handling"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Measured datasets often contain outlier data points which show totally wrong results.  This can be due to unforeseen events influencing the measurement.  Now we will investigate how to deal with these situations.\n",
    "\n",
    "First, we create a copy of the measurement data `y_noisy` from before, and set one point to a very wrong value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "y_o = y_noisy.copy()\n",
    "y_o[16] = -2.2\n",
    "data_o = pd.DataFrame({'x': x, 'y': y_o})"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "plt.plot(x, y_true, 'b.', ms=10, label=('true'))\n",
    "plt.plot(x, y_o, 'rv', ms=10, label=('noisy'))\n",
    "plt.legend(loc=0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When performing linear regression on these noisy data, the result will be significantly influenced by the outlier:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "m, b, r, p, stderr = linregress(x, y_o)\n",
    "print(m, b)\n",
    "plt.plot(x, y_true, 'b.', ms=10, label=('true'))\n",
    "plt.plot(x, y_o, 'rv', ms=10, label=('noisy'))\n",
    "plt.plot(x, m * x + b, 'c-', lw=2, label=('fitted'))\n",
    "plt.legend(loc=0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will look at three different ways of dealing with this situation: a) ignoring the outliers altogether, b) assigning weights to individual data points, and c) using robust regression."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Missing data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Performing regression on datasets containing `NaN` values fails:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "y_o[16] = np.nan\n",
    "linregress(x, y_o)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "But both `numpy.polyfit` and `scipy.stats.linregress` have versions which can work with *masked arrays*:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from scipy.stats.mstats import linregress as mlinregress\n",
    "import numpy.ma as ma"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "y_o_masked = ma.masked_invalid(y_o)\n",
    "mlinregress(x, y_o_masked)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exercise\n",
    "1. Verify that the `scipy.stats.mstats.linregress` function yields the same results as `numpy.ma.polyfit`.\n",
    "2. Compare the standard errors of the two fits (all data and outlier-removed data).\n",
    "3. Plot both fits and the original data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "m, b, r, p, stderr = mlinregress(x, y_o_masked)\n",
    "print(m, b)\n",
    "plt.plot(x, y_true, 'b.', ms=10, label=('true'))\n",
    "plt.plot(x, y_o, 'rv', ms=10, label=('noisy'))\n",
    "plt.plot(x, m * x + b, 'c-', lw=2, label=('fitted'))\n",
    "plt.legend(loc=0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Note:** *statsmodels* can handle `NaN` values without any special treatment:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "data_o['y'][16] = np.nan\n",
    "lm = ols(formula='y ~ x', data=data_o).fit()\n",
    "print(lm.params)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Weighted regression"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exercise\n",
    "A middle ground between using all data as-is and ignoring outliers would be to assign *weights* to each measurement.  Ideally, these weights are related to the uncertainties of the individual measurements, such that the errors of the products `weight[i]*y[i]` all have the same variance.\n",
    "\n",
    "Use the `w` parameter of the `numpy.polyfit` function to assign a lower weight to the outlier, and compare the results to the previous fits"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Note:** in *statsmodels*, there is `statsmodels.formula.api.wls`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Robust regression"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are statistical methods to properly deal with outliers; this is called [robust regression](https://en.wikipedia.org/wiki/Robust_statistics). *statsmodels* includes this, and the usage is identical to the \"normal\" linear model:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "from statsmodels.formula.api import rlm"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "data_o = pd.DataFrame({'x': x, 'y': y_noisy})\n",
    "data_o['y'][16] = -2.2\n",
    "# create a fitted model in one line\n",
    "rm = rlm(formula='y ~ x', data=data_o).fit()\n",
    "\n",
    "# print the coefficients\n",
    "print(rm.params)\n",
    "rm.summary()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exercise\n",
    "1. Compare the results from the robust regression to those of the previous two fits (normal fit and outlier-removed fit).\n",
    "2. Create a plot showing all three fits and the original data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Note:** *Seaborn* can also do robust regression, by specifying `robust=True` as kwarg:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "sns.lmplot(y='y', x='x', data=data_o, robust=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# More general curve fitting"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Linear regression is just a special case of *curve fitting*, which allows us to generally fit any function to data points.\n",
    "\n",
    "Suppose that we measure data which follows a *Gaussian distribution*, which is determined by the parameters $\\mu$, $\\sigma$, and a scaling factor $A$:\n",
    "\n",
    "$$ f(x) = A \\cdot \\frac{1}{\\sigma \\cdot \\sqrt{2 \\pi}} \\cdot \\exp \\frac{(x - \\mu)^2}{2 * \\sigma ^2} $$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "MU = 48.\n",
    "SIGMA = 9.2\n",
    "A = 3.4\n",
    "\n",
    "X = np.linspace(0, 100, 201)\n",
    "Y = A / (SIGMA * np.sqrt(2. * np.pi)) * np.exp(- (X - MU)**2 / (2. * SIGMA**2))\n",
    "\n",
    "plt.plot(X, Y, 'r-', lw=2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let us suppose that we measure 20 data points, and the measurements are affected by noise:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "x0 = X[::10]\n",
    "np.random.seed(123)\n",
    "y_noisy = Y[::10] + np.random.normal(scale=0.006, size=x0.size)\n",
    "\n",
    "plt.plot(X, Y, 'r-', lw=2, label='truth')\n",
    "plt.plot(x0, y_noisy, 'cv', ms=10, label='noisy')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can fit a curve to these data using the function `scipy.optimize.curve_fit`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from scipy.optimize import curve_fit\n",
    "help(curve_fit)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, we need to define the function $f(x)$.  It has to take $x$ as the first argument; the function's parameters $\\mu$, $\\sigma$, and $A$ follow:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def f(x, mu, sigma, a):\n",
    "    return (a / (sigma * np.sqrt(2. * np.pi)) *\n",
    "            np.exp(- (x - mu)**2 / (2. * sigma**2)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "popt, pcov = curve_fit(f, x0, y_noisy)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The parameters are then contained in the first return value `popt` of the `curve_fit` function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print('mu, sigma, a (simple fit): ', popt)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "mu, sigma, scale = popt\n",
    "plt.plot(X, Y, 'r-', lw=2, label='truth')\n",
    "plt.plot(x0, y_noisy, 'cv', ms=10, label='noisy')\n",
    "plt.plot(X, f(X, *popt), 'b-', ms=10, label='fitted')\n",
    "plt.legend()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This result is obviously very wrong.  Curve fitting is a tricky business ...\n",
    "\n",
    "In many situations it is helpful to have a *first guess* of the parameters:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In other situations, it can help to provide `curve_fit` with the valid ranges of the individual parameters.  This should be done to eliminate unphysical parameters (for example, negative $\\sigma$)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exercise\n",
    "1. What would be valid parameter ranges in our case?\n",
    "2. Use these parameter ranges in the `curve_fit` call.  **Hint:** look at the `bounds` parameter of the `curve_fit` function. *NumPy* has the special value `np.inf` to specify $\\infty$.\n",
    "3. Again, compare the results to previous fits."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exercise\n",
    "Use a first guess for the $\\sigma$ parameter in the `curve_fit` function.  **Hint:** check out the `p0` parameter to `curve_fit`.  Compare the results to the fit without a first guess."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Note:** In practice, you should always supply both initial guess and bounds."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# References\n",
    "- http://www.scipy-lectures.org/packages/statistics/ (linear regression)\n",
    "- http://www.scipy-lectures.org/advanced/mathematical_optimization/ (curve fitting)\n",
    "- https://github.com/justmarkham/DAT4/blob/master/notebooks/08_linear_regression.ipynb (advanced lin. regression)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
